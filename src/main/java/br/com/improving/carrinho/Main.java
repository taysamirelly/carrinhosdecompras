package br.com.improving.carrinho;

import java.math.BigDecimal;
import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {	long codigo = 124;
	long codigo2 = 234;
	long codigo3 = 105;

	BigDecimal valorUnitario = new BigDecimal(1.093);
	BigDecimal valorUnitario2=new BigDecimal(1.5);
	BigDecimal valorUnitario3=new BigDecimal(1.95);
	Produto produto = new Produto(codigo, "melao");
	Item item = new Item(produto, valorUnitario, 2);
	Produto produto2 = new Produto(codigo2, "melancia");
	Item item2 = new Item(produto2, valorUnitario2, 4);
	Produto produto3 = new Produto(codigo3, "abacaxi");
	Item item3 = new Item(produto3, valorUnitario3, 1);

	CarrinhoCompras carrinho = new CarrinhoCompras();
	ArrayList<Item> listaItem = (ArrayList<Item>) carrinho.getItens();
	

	carrinho.adicionarItem(produto, valorUnitario, 2);
	carrinho.adicionarItem(produto2, valorUnitario, 4);
	carrinho.adicionarItem(produto3, valorUnitario, 1);
	
	carrinho.removerItem(produto3, valorUnitario, 1);
	carrinho.removerItem(1);
	
	CarrinhoComprasFactory fac = new CarrinhoComprasFactory();
	fac.criar("Taysa");
	fac.invalidar("lucas");
	fac.getValorTicketMedio();
	
	}
}

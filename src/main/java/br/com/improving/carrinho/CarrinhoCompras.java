package br.com.improving.carrinho;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Classe que representa o carrinho de compras de um cliente.
 */
public class CarrinhoCompras {
	ArrayList<Item> listaItem = new ArrayList<Item>();
	Item item;
	int contador = 0;

	/**
	 * Permite a adição de um novo item no carrinho de compras.
	 * <p>
	 * Caso o item já exista no carrinho para este mesmo produto, as seguintes regras deverão ser
	 * seguidas: - A quantidade do item deverá ser a soma da quantidade atual com a quantidade
	 * passada como parâmetro. - Se o valor unitário informado for diferente do valor unitário atual
	 * do item, o novo valor unitário do item deverá ser o passado como parâmetro.
	 * <p>
	 * Devem ser lançadas subclasses de RuntimeException caso não seja possível adicionar o item ao
	 * carrinho de compras.
	 *
	 * @param produto
	 * @param valorUnitario
	 * @param quantidade
	 */
	public void adicionarItem(Produto produto, BigDecimal valorUnitario, int quantidade) {
		try {
			item = new Item(produto, valorUnitario, quantidade);
			if (produto.equals(getItens())) {
				quantidade = quantidade + item.getQuantidade();
				if (!valorUnitario.equals(item.getValorUnitario())) {
					valorUnitario = item.getValorUnitario();
				}
				item = new Item(produto, valorUnitario, quantidade);
			}
			listaItem.add(item);
			System.out.println("Item adicionado!");
			contador++;
		} catch (RuntimeException erro) {
			System.err.println("Não foi possível adicionar o item!");
		}
	}

	/**
	 * Permite a remoção do item que representa este produto do carrinho de compras.
	 *
	 * @param produto
	 * @return Retorna um boolean, tendo o valor true caso o produto exista no carrinho de compras e
	 *         false caso o produto não exista no carrinho.
	 */
	public boolean removerItem(Produto produto, BigDecimal valorUnitario, int quantidade) {
		try {
		item = new Item(produto,valorUnitario,quantidade);
		 for(int i = 0; i < listaItem.size(); i++)
		    {
		        if(item.getProduto().equals(produto))
		        {
		            listaItem.remove(item);
		            contador--;
		            System.out.println("item removido!");
		            return true;
		        }
		    }
		}catch (IndexOutOfBoundsException e){
		        	 System.err.println("Não foi possível remover o item!");
		        }

		 return false;
		    }
		 

	/**
	 * Permite a remoção do item de acordo com a posição. Essa posição deve ser determinada pela
	 * ordem de inclusão do produto na coleção, em que zero representa o primeiro item.
	 *
	 * @param posicaoItem
	 * @return Retorna um boolean, tendo o valor true caso o produto exista no carrinho de compras e
	 *         false caso o produto não exista no carrinho.
	 */
	public boolean removerItem(int posicaoItem) {
		try {
		 for(int i = 0; i < listaItem.size(); i++)
		 {
		            listaItem.remove(posicaoItem);
		            contador--;
		            System.out.println("item removido!");
		            return true;
		    }
		 }catch(IndexOutOfBoundsException e) {
			 System.err.println("Não foi possível remover o item!");
		 }
		
		 return false;
	}
		 



	/**
	 * Retorna o valor total do carrinho de compras, que deve ser a soma dos valores totais de todos
	 * os itens que compõem o carrinho.
	 *
	 * @return BigDecimal
	 */
	public BigDecimal getValorTotal() {
		return item.getValorTotal();
	}

	/**
	 * Retorna a lista de itens do carrinho de compras.
	 *
	 * @return itens
	 */
	public Collection<Item> getItens() {
		return listaItem;
	}
}

package br.com.improving.carrinho;

import java.math.BigDecimal;


/**
 * Classe que representa um item no carrinho de compras.
 */
public class Item {

	private Produto produto=new Produto(124099L,"lapis");;
	private BigDecimal valorUnitario= new BigDecimal(2);
	private int quantidade=3;


	/**
	 * Construtor da classe Item.
	 *
	 * @param produto
	 * @param valorUnitario
	 * @param quantidade
	 */
	public Item(Produto produto, BigDecimal valorUnitario, int quantidade) {
		//inicializa os atributos no construtor
		this.produto = produto;
		this.valorUnitario = valorUnitario;
		this.quantidade = quantidade;

	}
	public Item() {
		//construtor alternativo

	}

	/**
	 * Retorna o produto.
	 *
	 * @return Produto
	 */
	public Produto getProduto() {
		return this.produto;
	}

	/**
	 * Retorna o valor unitário do item.
	 *
	 * @return BigDecimal
	 */
	public BigDecimal getValorUnitario() {
		return this.valorUnitario;
	}

	/**
	 * Retorna a quantidade dos item.
	 *
	 * @return int
	 */
	public int getQuantidade() {
		return this.quantidade;
	}

	/**
	 * Retorna o valor total do item.
	 *
	 * @return BigDecimal
	 */
	public BigDecimal getValorTotal() {
		//faz a multiplicação entre a quantidade e o valor total para dar o valor total
		if(valorUnitario==null) {
			valorUnitario= new BigDecimal(1);
		}if(quantidade==0){
			quantidade=1;
		}
		int unidade=valorUnitario.intValue();
		int valorTotal=unidade*getQuantidade();
		BigDecimal mulResul = BigDecimal.valueOf(valorTotal);
		return mulResul;

	}
}

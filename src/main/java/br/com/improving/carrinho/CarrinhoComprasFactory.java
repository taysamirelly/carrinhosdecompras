package br.com.improving.carrinho;

import java.math.BigDecimal;

/**
 * Classe responsável pela criação e recuperação dos carrinhos de compras.
 */
public class CarrinhoComprasFactory {
	String identricacaoCliente;
	Produto produto=new Produto(124099L,"lapis");
	BigDecimal valorUnitario= new BigDecimal(2);
	int quantidade;
	Item item= new Item(produto,valorUnitario,quantidade);
	CarrinhoCompras carrinho;
	int contador=0;

	

	/**
	 * Cria e retorna um novo carrinho de compras para o cliente passado como parâmetro.
	 * <p>
	 * Caso já exista um carrinho de compras para o cliente passado como parâmetro, este carrinho
	 * deverá ser retornado.
	 *
	 * @param identificacaoCliente
	 * @return CarrinhoCompras
	 */
	public CarrinhoCompras criar(String identificacaoCliente) {
		this.identricacaoCliente = identificacaoCliente;
		try {
		CarrinhoCompras carrinho = new CarrinhoCompras();
		if (identificacaoCliente != null) {
			carrinho = new CarrinhoCompras();
			System.out.println("Carrinho criado!");
			contador++;
		}
		}catch (RuntimeException erro) {
			System.err.println("Não foi possível criar o carrinho!");
		}
		
		return carrinho;
	}

	/**
	 * Retorna o valor do ticket médio no momento da chamada ao método. O valor do ticket médio é a
	 * soma do valor total de todos os carrinhos de compra dividido pela quantidade de carrinhos de
	 * compra. O valor retornado deverá ser arredondado com duas casas decimais, seguindo a regra:
	 * 0-4 deve ser arredondado para baixo e 5-9 deve ser arredondado para cima.
	 *
	 * @return BigDecimal
	 */
	public BigDecimal getValorTicketMedio() {
		//faz o calculo do valor médio do ticket
		BigDecimal cont= new BigDecimal(contador);
		BigDecimal valorTotal = item.getValorTotal().divide(cont);
		int a=valorTotal.intValue();
		int arredonda = Math.round(a);
		BigDecimal arrendondado= new BigDecimal(arredonda);
		System.out.println("O valor médio do ticket é: "+arrendondado);
		return valorTotal;
	}

	/**
	 * Invalida um carrinho de compras quando o cliente faz um checkout ou sua sessão expirar. Deve
	 * ser efetuada a remoção do carrinho do cliente passado como parâmetro da listagem de carrinhos
	 * de compras.
	 *
	 * @param identificacaoCliente
	 * @return Retorna um boolean, tendo o valor true caso o cliente passado como parämetro tenha um
	 *         carrinho de compras e e false caso o cliente não possua um carrinho.
	 */
	public boolean invalidar(String identificacaoCliente) {
		//verifica se o cliente possui carrinho
		if (identificacaoCliente != null) {
			System.out.println("Cliente não possui carrinho");
			return false;
		}
		System.out.println("Cliente possui carrinho");
		return true;
	}
}
